-- chikun :: 2014
-- Player object


-- Create player table
player = { }


-- For player creation
function player:create(px, py, pw, ph)

    -- Implement player variable
    player.x         = px
    player.y         = py
    player.w         = pw
    player.h         = ph
    player.ySpeed    = 0
    player.canJump   = false
    player.previous  = player

end


-- On player update
function player:update(dt)

    -- Calculate horizontal movement
    local xMove = 0

    if input.check("left") then
        xMove = xMove - 1
    end
    if input.check("right") then
        xMove = xMove + 1
    end

    -- Calculate y speed
    player.ySpeed = player.ySpeed + gravity * dt

    -- Move player on horizontal axis
    player.x = player.x + 256 * xMove * dt

    -- Prevent player from moving inside of blocks
    while math.overlapTable(collision, player) do
        player.x = math.round(player.x) - xMove
    end

    -- Make player jump if can
    if input.check("action") and player.canJump then
        player.ySpeed = -(gravity/2)
    end

    -- Move player on vertical axis
    player.y = player.y + player.ySpeed * dt

    -- Prevent player moving inside blocks
    if math.overlapTable(collision, player) then
        player.y = math.ceil(player.y)
        while math.overlapTable(collision, player) do
            player.y = player.y - math.sign(player.ySpeed)
        end
        --Reset ability to jump if fell onto object
        if player.ySpeed > 0 then
            player.canJump = true
        end
        player.ySpeed = 0
    end

    -- If player is falling then prevent jumping
    if  player.ySpeed ~= 0 then
        player.canJump = false
    end

end


-- On player draw
function player:draw()

    -- Draw basic player
    g.setColor(200, 150, 100)
    g.rectangle("fill", player.x, player.y, player.w, player.h)

end
