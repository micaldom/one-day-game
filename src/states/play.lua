-- chikun :: 2014
-- Play state


-- Temporary state, removed at end of script
local newState = { }
enemy  = { }
lvlEnd = { }

-- On state create
function newState:create()

    -- Load first map
    map.current = map.load("lvl" .. lvl)

    -- Go through layers in map
    for key, layer in ipairs(map.current.layers) do
        --Interpret important layer
        if layer.name == "important" then
            for key, object in ipairs(layer.objects) do
                -- Place player at the start
                if object.name == "playerStart" then
                    checkpoint = object
                elseif object.name == "finish" then
                    lvlEnd[#lvlEnd + 1] = object
                end
            end
        elseif layer.name == "collisions" then
            collision = layer.objects
        elseif layer.name == "enemies" then
            for key, object in ipairs(layer.objects) do
                enemySpawn1(object)
            end
        end
    end

    -- Create player based on checkpoint
    player:create(checkpoint.x, checkpoint.y, checkpoint.w, checkpoint.h)

    -- Get map size
    mapW = map.current.w * map.current.tileW
    mapH = map.current.h * map.current.tileH

    -- Create view object
    view = {
        x = 0,
        y = 0,
        w = w.getWidth(),
        h = w.getHeight()
    }

end


-- On state update
function newState:update(dt)

    -- Update view
    view.x = math.clamp(0, player.x + (player.w / 2) - (view.w / 2), mapW - view.w)
    view.y = math.clamp(0, player.y + (player.h / 2) - (view.h / 2), mapH - view.h)

    -- Update player
    player:update(dt)

    -- Reset player if fall off the bottom of the screen
    if player.y >= view.y + view.h then
        state.change(states.play)
    end

    -- Move to next lvl if player touches finish
    for key, object in ipairs(lvlEnd) do
        if math.overlap(object, player) then
            lvl = lvl + 1
            state.change(states.play)
        end
    end

    enemy.update(dt)

end


-- On state draw
function newState:draw()

    -- Reset colour
    g.setColor(255, 255, 255)

    -- Move graphics based on view
    g.translate(math.round(-view.x), math.round(- view.y))

    -- Draw the current lvl
    map.draw()

    -- Draw lvlEnd
    g.setColor(0, 0, 255)
    for key ,object in ipairs(lvlEnd) do
        g.rectangle("fill", object.x, object.y, object.w, object.h)
    end

    -- Draw the player
    player:draw()

    -- Draw the enemies
    enemy.draw()

    --Reset graphical translation
    g.origin()

end


-- On state kill
function newState:kill()

    enemies = { }
    lvlEnd  = { }

end


-- Transfer data to state loading script
return newState
