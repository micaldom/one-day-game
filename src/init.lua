-- chikun :: 2014
-- Load required libraries


-- Load shorthand bindings
require "src/bindings"

-- Load maths functions
require "src/maths"

-- Load miscellaneous functions
require "src/misc"

-- Load core functions
require "src/core"

-- Load graphics from folder recursively
require "src/loadGFX"

-- Load sounds from folder recursively
require "src/loadSND"

-- Load states from folder recursively
require "src/loadStates"

-- Load map functions
require "src/mapTool"

-- Load state manager
require "src/stateManager"

-- Load input manager
require "src/input"

-- Load player functions
require "src/player"

-- Load enemies functions
require "src/enemy"
