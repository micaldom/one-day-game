-- chikun :: 2014
-- Code for making enemies do stuff


enemy = { }
enemies = { }

function enemy.update(dt)

    --make all enemies move at once
    for key, enemy in ipairs(enemies) do
        -- Calculate y speed
        enemy.ySpeed = enemy.ySpeed + gravity * dt

        -- Move enemy on horizontal axis
        enemy.x = enemy.x + 256 * enemy.xMove * dt

        local value = false

        -- Prevent enemy from moving inside of blocks
        while math.overlapTable(collision, enemy) do
            value = true
            enemy.x = math.round(enemy.x) - enemy.xMove
        end

        -- Reverse direction
        if value and not math.overlapTable(collision, enemy) then
            enemy.xMove = - enemy.xMove
            value = false
        end

        -- Bounce enemies off of each other
        for key, enemies in ipairs(enemies) do

            if enemy ~= enemies then

                while math.overlap(enemy, enemies) do
                    value = true
                    enemy.x = math.round(enemy.x) - enemy.xMove
                end

                --Reverse direction if collised with enemy
                if value and not math.overlap(enemy, enemies) then
                    enemy.xMove = - enemy.xMove
                    value = false
                end
            end
        end

        -- Temporary enemy to prevent falling off of objects
        local tempEnemy = {
            x = enemy.x + 32 * enemy.xMove,
            y = enemy.y + 32,
            w = enemy.w,
            h = enemy.h
        }

        if math.overlapTable(collision, tempEnemy) then
            value = true
        end

        if not value then
                enemy.xMove = - enemy.xMove
        end

        -- Move enemy on vertical axis
        enemy.y = enemy.y + enemy.ySpeed * dt

        -- Prevent enemy moving inside blocks
        if math.overlapTable(collision, enemy) then
            enemy.y = math.ceil(enemy.y)
            while math.overlapTable(collision, enemy) do
                enemy.y = enemy.y - math.sign(enemy.ySpeed)
            end
            enemy.ySpeed = 0
        end

        -- Kill enemy if player jumps on their head
        if math.overlap(player, enemy) then
            if player.ySpeed > 0 then
                table.remove(enemies, key)
                player.ySpeed = - gravity/2
            else
                state.change(states.play)
            end
        end
    end
end


function enemy.draw()

    g.setColor(255, 0, 0)

    -- Draw all enemies that exist
    for key, object in ipairs(enemies) do
        g.rectangle("fill", object.x, object.y, object.w, object.h)
    end

end

-- Set values for the enemies
function enemySpawn1(obj)
    enemies[#enemies + 1]     = obj
    enemies[#enemies].xMove  = 1
    enemies[#enemies].ySpeed = 0
end
