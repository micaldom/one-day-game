-- chikun :: 2014
-- Core routines


core = { }      -- Create core table


function core.load()

    -- Load global variables required

    -- Set gravity
    gravity = 1024

    -- Set initial lvl
    lvl = 1

    -- Load play state
    state.load()

end


function core.update(dt)

    -- Updated global variables

end


function core.draw()

    -- Draw overlay

end
